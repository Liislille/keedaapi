package com.liislille.keeda.model;

public class Recipe {

    private int id;
    private String mealName;
    private String timeCategory;
    private String mealCategory;
    private String recipe;

    public Recipe(int id, String mealName, String timeCategory, String mealCategory, String recipe) {
        this.id = id;
        this.mealName = mealName;
        this.timeCategory = timeCategory;
        this.mealCategory = mealCategory;
        this.recipe = recipe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public String getTimeCategory() {
        return timeCategory;
    }

    public void setTimeCategory(String timeCategory) {
        this.timeCategory = timeCategory;
    }

    public String getMealCategory() {
        return mealCategory;
    }

    public void setMealCategory(String mealCategory) {
        this.mealCategory = mealCategory;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }
}
