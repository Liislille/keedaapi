package com.liislille.keeda.controller;


import com.liislille.keeda.model.Recipe;
import com.liislille.keeda.repository.KeedaRepository;
import com.liislille.keeda.service.KeedaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/recipe")
@CrossOrigin("*")
public class KeedaController {

    @Autowired
    private KeedaRepository keedaRepository;

    @Autowired
    private KeedaService keedaService;

    @GetMapping
    public List<Recipe> getRecipes() {
        return keedaRepository.fetchAllRecipes();
    }

    @GetMapping("/{mealCategory}/{timeCategory}")
    public Recipe getRandomRecipe(@PathVariable String mealCategory, @PathVariable int timeCategory) {
        return keedaRepository.findRandomRecipe(mealCategory, timeCategory);
    }

}
