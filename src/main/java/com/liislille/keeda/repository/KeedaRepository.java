package com.liislille.keeda.repository;

import com.liislille.keeda.model.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class KeedaRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;



   public List<Recipe> fetchAllRecipes() {
        return jdbcTemplate.query("select * from retseptid", (dbRow, sequenceNumber) -> {
                    return new Recipe(dbRow.getInt("id"), dbRow.getString("mealName"), dbRow.getString("timeCategory"), dbRow.getString("mealCategory"), dbRow.getString("recipe"));
                }
        );
    }

   public Recipe findRandomRecipe(String mealCategory, int timeCategory) {
        List<Recipe> recipes = jdbcTemplate.query("SELECT * FROM retseptid where mealCategory = ? and timeCategory <= ? ORDER BY rand() LIMIT 1",
                new Object[]{mealCategory, timeCategory}, (dbRow, sequenceNumber) ->
                { return new Recipe(dbRow.getInt("id"), dbRow.getString("mealName"),
                        dbRow.getString("timeCategory"), dbRow.getString("mealCategory"),
                        dbRow.getString("recipe"));

                }
        );
        if(recipes.size() >0){
            return recipes.get(0);

        }
        return null;

    }
}

