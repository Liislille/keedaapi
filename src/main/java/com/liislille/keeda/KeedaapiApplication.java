package com.liislille.keeda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeedaapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeedaapiApplication.class, args);
	}

}
